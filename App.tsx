import React from 'react';
import Home from './src/Screens/Home';
import { Provider as ReduxProvider } from 'react-redux'
import store from './src/Library/Store';

const App = () => {

  return (
    <ReduxProvider store={store}>
      <Home />
    </ReduxProvider>

  );
};


export default App;