<h1>NextToGo</h1>

---

<ul>
  <li>React Native Redux Application</li>
</ul>

---
 
<h2>Task</h2>
 
Create a single page application that displays 'Next to go’ races using our API.

A user should see 5 races at all times, and they should be sorted by time ascending. Race
should disappear from the list after 1 min past the start time (​advertised_start).  

User should see meeting name (​meeting_name), race number (​race_number) and 
countdown timer that indicates the start of the race.

User should be able to toggle race categories to view races belonging to only the selected
category. 

Categories are defined by IDs and are the following. 

<ul>
  <li>Greyhound racing: ​category_id: '9daef0d7-bf3c-4f50-921d-8e818c60fe61'</li>
  <li>Harness racing: ​category_id: '161d9be2-e909-4326-8c2c-35ed71fb460b' </li>
  <li>Horse racing: ​category_id: '4a2788f8-e825-4d36-9894-efd4baf1cfae' </li>
</ul>


<h2>Objectives</h2>

<ul>
  <li>Build a React Native Application (https://facebook.github.io/react-native)</li>
  <li>Use Neds API to fetch a list of races :heavy_check_mark: </li>
  <li>Use redux                             :heavy_check_mark: </li>
  <li>Unit tests                            :x:                </li>
  <li>Documentation                         :heavy_check_mark: </li>
</ul>


## :camera: Screenshots
<p float="left" padding="0 20px">
    <img src=".github/sc_1.png"  width="200" height="400" />
    <img src=".github/sc_2.png"  width="200" height="400" />
    <img src=".github/sc_3.png"  width="200" height="400" />
    <img src=".github/sc_4.png"  width="200" height="400" />
</p>



## Project structure

* 📁 `src`: Contains the actual TypeScript + React (-Native) front-end for the Colorwaver App.
* 📁 `API`: Contain Axios Api functions and Main Api End Point
* 📁 `Components`: Contain all UI Components
* 📁 `Library`: Contain Redux Store
* 📁 `Screens`: Contain Apps Home screen
* 📁 `Slice`: Contain Redux Slice
* 📁 `Utils`: Contain Global Styles, Theme constants and Helper functions

## Base dependencies

* axios for networking
* @reduxjs/toolkit for State Management

## Prerequisites

* "react": "17.0.2"
* "react-native": "0.68.1"
* "@reduxjs/toolkit": "1.8.1"
* "axios": "0.26.1"
* "react-redux": "8.0.0"
* "redux-logger": "3.0.6"


## Installation

* `git clone https://mudaligek@bitbucket.org/mudaligek/nexttogo.git`
* `cd nextToGo`
* `yarn`
* `npx react-native run-android`

## Platforms

* Android :heavy_check_mark:
* IOS     :x:

 
 
 


