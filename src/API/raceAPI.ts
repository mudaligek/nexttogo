import axios from 'axios';
import React from 'react';
import { RaceInfo } from '../Slice/Types';

//Main Url End point 
const MainURL = 'https://api.neds.com.au/rest/v1';


/**
 *Get Race list api method 
 * @param {number} - count - this is limit of record that we need to down at onetime 
 * @returns Promise
 */

export const GETRaceList = async (count: number) => {

        let result = await axios.get(`${MainURL}/racing/?method=nextraces&count=${count}`)
        return result;

}