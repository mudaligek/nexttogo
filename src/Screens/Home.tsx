import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    StatusBar,
    StyleSheet,
    useColorScheme,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import CategoryContainerComponent from '../Components/CategoryContainer';
import CustomError from '../Components/ErrorComponent';
import RaceListComponent from '../Components/RaceList';
import TitleComponent from '../Components/Title';
import { RootState } from '../Library/Store';
import { Colors } from '../Utils/Theme';
import { fetchRaceList } from '../Slice/Race'

/**
 * Apps Initial Screen
 * this will hold Title, Category,RaceList,Custome Error Components
 * @returns Element
 */

const Home = () => {
    //Get Screen color scheam to set background color
    const isDarkMode = useColorScheme() === 'dark';

    const dispatch = useDispatch();
    //Redux Race state 
    const ScreenState = useSelector((state: RootState) => state.Race);
    //initialize Error Modal Visible state
    const [ErrorModalvisible, SetErrorModalVisible] = useState<boolean>(ScreenState.Error);
    //set background color according to selected color scheam
    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.Dark : Colors.Light,
    };

   

    /**
     * Close Error modal
     */
    const _onPressModalclose = () => {
        //Hide error modal
        SetErrorModalVisible(false)
        //calculate new race limit to download
        let newLimit = ScreenState.race_summaries?.length + 10
        //dispatch redux reducer to retrive fetch list limit of new limit
        dispatch(fetchRaceList(newLimit));


    }

    return (
        <SafeAreaView style={[backgroundStyle, styles.container]}>
            <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />

            <TitleComponent
                Title='Next to go'
            />
            <CategoryContainerComponent
                Title={"Race"}
            />
            <CustomError
                visible={ErrorModalvisible}
                onPressModalclose={_onPressModalclose}
            />
            <RaceListComponent />

        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
});

export default Home;
