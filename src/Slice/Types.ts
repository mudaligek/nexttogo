/**
 * Types that includes in Race Slice
 */

export type CategoryListItem = {
    name: string,
    category_id: string,
    status: boolean,
    img_url:any
}

export type RaceInitialState = {
    RaceInfo: {},
    race_summaries:race_summarie[],
    Loading: 'idle' | 'pending' | 'succeeded' | 'failed',
    Error: boolean,
    ErrorMessage?: string,
    CategoryList: CategoryListItem[]
}

export type ServerError = {
    statusCode:number;
    message: string;
}

type RaceInfoData={
    next_to_go_ids:string;
    race_summaries:race_summaries;
}

type advertised_start={
    seconds:number
}

export type race_summarie={
    race_id:string,
    race_name:string,
    race_number:number,
    meeting_id:string,
    meeting_name:string,
    category_id:string,
    advertised_start:advertised_start
}

export type race_summaries={
    [key:string]:race_summarie
}
export type RaceInfo={
    status:number,
    message:string,
    data:RaceInfoData,
    
}

export type TUpdateCategoryStatus = {
    category_id: string,
    status: boolean
}

export type TRemoveRaceFromList = {
    race_id?: string
}