/**
 * This is Race Slice for Redux Toolkit state handling
 * 
 * CreateAsyncThunk - FetchRaceList
 * 
 * Reducers
 * SetCategoryStatus - Toggle between categories
 * @param { string } - category_id
 * 
 * RemoveRaceFromList - Remove Completed Races
 * 
 * Extra Reducers
 * FetchRaceList.pending
 * FetchRaceList.fulfilled
 * FetchRaceList.rejected
 * 
 */

import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit';
import { AxiosError, } from 'axios';
import { GETRaceList } from '../API/raceAPI';
import { AppDispatch } from '../Library/Store';
import { RaceInfo, RaceInitialState, race_summarie, ServerError, TRemoveRaceFromList, TUpdateCategoryStatus } from './Types';


//create a thunk to retrive racelist
export const fetchRaceList = createAsyncThunk<
    RaceInfo,
    number,
    {
        dispatch:AppDispatch,
        rejectValue: ServerError
    }
>(
    'race/fetchRacesByCount',
    async (count: number, thunkAPI) => {
        // call Get race api wich hadle axios method and required limit of races to download
        const response = await GETRaceList(count);

        if (response.status != 200) {

            return thunkAPI.rejectWithValue({
                message: 'Failed to fetch Race Info',
                statusCode: response.status
            })
        }

        return response.data;

    }
);

//initial race state
//Category List hard coded as requirment
const initalState: RaceInitialState = {
    RaceInfo: {},
    race_summaries: [],
    Loading: 'idle',
    Error: false,
    ErrorMessage: '',
    CategoryList: [
        {
            name: "Greyhound",
            category_id: "9daef0d7-bf3c-4f50-921d-8e818c60fe61",
            status: false,
            img_url: require('../Components/img/Greyhound_racing.png')
        },
        {
            name: "Harness",
            category_id: "161d9be2-e909-4326-8c2c-35ed71fb460b",
            status: false,
            img_url: require('../Components/img/Harness_racing.png')
        },
        {
            name: "Horse",
            category_id: "4a2788f8-e825-4d36-9894-efd4baf1cfae",
            status: false,
            img_url: require('../Components/img/Horse_racing.png')
        }
    ]

}


//create slices
const RaceState = createSlice({
    name: 'Race',
    initialState: initalState,
    reducers: {

        SetCategoryStatus: (state, action: PayloadAction<TUpdateCategoryStatus>) => {
            //reducer-Setcategory state enable or disable 
            state.CategoryList.map(m => {
                if (m.category_id == action.payload.category_id) {
                    m.status = action.payload.status;
                }
            })
        },
        RemoveRaceFromList: (state, action: PayloadAction<TRemoveRaceFromList>) => {
            //remove completed raced from state
            let copyList = [...state.race_summaries];
            let filtredList = copyList.filter(f => f.race_id != action.payload.race_id);

            state.race_summaries = filtredList;

        }
    },
    extraReducers: (builder) => {
        builder.addCase(fetchRaceList.pending, (state) => {
            //RaceList Loading reducers to update loading state
            state.Loading = 'pending';
            state.Error = false;
            state.ErrorMessage = "";
        }),
            builder.addCase(fetchRaceList.fulfilled, (state, action) => {
                //RaceList fultilled reducers to update race list state
                //create racelist array using racelist object list 
                let arr = action.payload?.data?.race_summaries ? Object.values(action.payload?.data?.race_summaries) : []
                //combine existing races and new ralist
                let FiltredArr:race_summarie[] = []
                //filter new race list using existing racelist
                //remove dublications from new racelist

                arr.map(m => {
                    let check = state.race_summaries.find(t => t.race_id == m.race_id)
                    if (!check) {
                        FiltredArr.push(m);
                    }

                })

                //combine existing racelist with new race list
                let newArr = [...state.race_summaries, ...FiltredArr]

                //assign new race list as redux state
                state.race_summaries = newArr
                state.Loading = "idle";

            }),
            builder.addCase(fetchRaceList.rejected, (state, action) => {
                //RaceList fultilled reducers to update error and loading state
                state.Error = true;
                state.Loading = 'pending';
                state.ErrorMessage = action.error?.message;
            })
    }
})

export const {
    SetCategoryStatus,
    RemoveRaceFromList
} = RaceState.actions;

export default RaceState.reducer;


