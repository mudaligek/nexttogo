export const Padding = {
    small : 12,
    medium : 24,
    large:36
}

export const FontSize = {
    small : 16,
    medium : 24,
    large:36
}

export const Colors = {
    primary:'#FF7800',
    secondary:'#1C9CEA',
    Light:'#ffffff',
    Dark:'#000000',
    success:'#58F924',
    error:'#D83D17',
    
}

export const Margin={
    small : 4,
    medium : 6,
    large:12
}

export const Radius={
    small : 12,
    medium : 24,
    large:48
}

export const Height={
    small : 40,
    medium : 50,
    large:60,
    SmallModalHeight:250
}