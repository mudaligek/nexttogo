
/**
 * Get Countdown string and timestamp difference based on Startime Time stamp and additional Minute
 *
 * @param {number} StartTimeStamp - console[level]
 * @param {number | null} addititionalMinutes - selected action
 *
 * @returns {object} Count down time and timestamp difference
 */
export const getTimeRemaning = (StartTimeStamp: number, addititionalMinutes?: number) => {

    var StartTime = new Date().getTime();
    var difference: number;
    var CurrentTime = new Date().getTime();

    if (addititionalMinutes) {
        var newDate = new Date(StartTimeStamp * 1000);
        //Add Additional minute to start time
        StartTime = new Date(newDate.getTime() + addititionalMinutes * 60000).getTime();
        // Find the distance between StartTime and the CurrentTime
        difference = StartTime - CurrentTime;
    } else {
        //Convert Unix time stam to JS datetime
        StartTime = new Date(StartTimeStamp * 1000).getTime();
        // Find the distance between StartTime and the CurrentTime
        difference = StartTime - CurrentTime;
    }


    // Time calculations for days, hours, minutes and seconds
    var minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((difference % (1000 * 60)) / 1000);


    return {
        difference,
        minutes,
        seconds
    };

}