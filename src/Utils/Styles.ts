import React from 'react';
import { StyleSheet, Platform } from 'react-native'
import { Padding } from './Theme';
/**
 * Global Styles
 */
const GlobalStyle = StyleSheet.create({

  card: {
    backgroundColor: 'white',
    borderRadius: 20,
    paddingVertical: 20,
    width: '80%'
  },
  cardSmall: {
    backgroundColor: 'white',
    borderRadius: 10,
  },
  shadow: {

    ...Platform.select({
      ios: {
        shadowColor: '#171717',
        shadowOffset: { width: -2, height: 4 },
        shadowOpacity: 0.2,
        shadowRadius: 3,
      },
      android: {
        elevation: 20
      }
    })


  }

});

export default GlobalStyle;