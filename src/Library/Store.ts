import { combineReducers, configureStore } from '@reduxjs/toolkit';
import Race from '../Slice/Race';
import logger from 'redux-logger';
import { useDispatch } from 'react-redux';



const rootReducer = combineReducers({
    Race:Race
})

export type RootState = ReturnType<typeof rootReducer>;




const store=configureStore({
    reducer:rootReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
})

export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();

export default store;