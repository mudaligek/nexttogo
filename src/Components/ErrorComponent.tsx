import React from 'react';
import {
    View, Text, StyleSheet, TouchableNativeFeedback, Modal
} from 'react-native';
import GlobalStyle from '../Utils/Styles';
import { Colors, FontSize, Height, Padding, Radius } from '../Utils/Theme';
import Button from './Button';

type Props = {
    visible: boolean,
    onPressModalclose: () => void;
}
/**
 * Custom Error component
 * this will display the custom error message 
 * @param {boolean} - visible - it will hadle modal visible or not
 * @param {()=>arg} -  onPressModalclose -user press event of try again button
 * @returns React Modal element
 */
const CustomError = ({ visible, onPressModalclose }: Props) => {


    return (
        <Modal
            transparent={true}
            animationType={'slide'}
            visible={visible}
        >
            <View style={styles.container}>
                <View style={[GlobalStyle.card, GlobalStyle.shadow, styles.errorContiner]}>
                    <View style={styles.header}>
                        <Text style={[styles.text, { fontWeight: 'bold' }]}>Error</Text>
                    </View>
                    <View style={styles.body}>
                        <Text style={[styles.text, { fontSize: FontSize.small }]}>Something went wrong.Please try again later.</Text>
                    </View>
                    <View style={styles.footer}>

                        <Button
                            onPress={onPressModalclose}
                            title='Try Again'
                        />
                    </View>



                </View>
            </View>
        </Modal>

    )
}

const styles = StyleSheet.create({

    header: {
        flex: 1, alignItems: 'center', justifyContent: 'center'
    },
    body: {
        flex: 1
    },
    footer: {
        flex: 1
    },
    container: {
        flex: 1, alignItems: 'center', justifyContent: 'center'
    },
    errorContiner: {
        // height: Height.medium,
        padding: Padding.medium,
        //backgroundColor: Colors.error,
        alignItems: 'center',
        justifyContent: 'center',
        height: Height.SmallModalHeight
    },
    text: {
        fontSize: FontSize.medium,
        fontWeight: '400',
        //color: 'white'
    }
})

export default CustomError;