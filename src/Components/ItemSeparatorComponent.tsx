import React from 'react';
import { View, StyleSheet,useWindowDimensions } from 'react-native';
import { Colors } from '../Utils/Theme';

const ItemSeparatorComponent =()=>{
    const { height, width } = useWindowDimensions();

    return (<View style={[styles.container,{ width:width,}]}></View>)
}

const styles = StyleSheet.create({
    container:{
       
        height:1,
        backgroundColor:'lightgray'
    }
})

export default ItemSeparatorComponent;