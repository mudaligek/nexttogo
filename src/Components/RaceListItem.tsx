import React, { useState } from 'react';
import {
    View, Text, StyleSheet, useColorScheme, Image, Pressable
} from 'react-native';
import GlobalStyle from '../Utils/Styles';
import { Colors, FontSize, Height, Margin, Padding, Radius } from '../Utils/Theme';
import CountDowncomponent from './CountDown';
import RaceNameComponent from './RaceName';


export type Props = {
    meeting_name: string;
    race_number: number;
    race_id: string;
    StartTime: number;

}

/**
 * Race List item display Race Name component and CountDown component
 * @param {string} - meeting_name 
 * @param {string} - race_number
 * @param {number} - StartTime - this will pass into countdown component to calculate countdown
 * @param {string} - race_id - this will pass into countdown component to remove completed Race from list
 * @returns Element with display Race Name component and CountDown component
 */

const RaceItemComponent: React.FC<Props> = ({ meeting_name, race_number, StartTime, race_id }: Props) => {
    //Get Screen color scheam to set background color
    const isDarkMode = useColorScheme() === 'dark';
    //set background color according to selected color scheam
    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.Dark : Colors.Light,

    };




    return (
        <View style={[styles.container, backgroundStyle]}>


            <RaceNameComponent
                meeting_name={meeting_name}
                race_number={race_number}
            />

            <CountDowncomponent
                race_id={race_id}
                StartTime={StartTime}
            />


        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: Height.large,
        margin: Margin.large,
        borderRadius: Radius.small,
        padding: Padding.small,
        justifyContent: 'space-between'
    },


});

export default RaceItemComponent;