import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useDispatch } from 'react-redux';
import { RemoveRaceFromList } from '../Slice/Race';
import { Colors, FontSize, Radius } from '../Utils/Theme';
import { getTimeRemaning } from '../Utils/Utils';

type Props = {
    StartTime: number,
    race_id: string
}


/**
 * CountDown component
 * handle Race CountDown Logic in Race ListItem Component and Show CountDown and AfterRaceStart Count Down
 * 
 * @param {number} - StartTime - this is timestamp of Starttime its coming as Unix timestamp with 10 digits
 * @param {string} - race_id - this holding race_id it will use to delete race after Count Down completed
 * @returns Race CountDown Element
 */
const CountDowncomponent = ({ StartTime, race_id }: Props) => {
    //mutable ref object to hold intervalId more efficient way
    //Tried to use let variable but didn't hold the interval id due to listview item
    const intervalId = useRef<NodeJS.Timeout|null>(null)
    /**
     * mutable ref object to hold race_id
     * can access race_id using current.RaceID
     */
    const intervalRaceId = useRef<string|null>(null)
    //initialize states for count downs
    //Hold Count Down String
    const [CountDown, SetCountDown] = useState<string>("");
    //Hold After Race start Count Down
    const [WaitingCountDown, SetWaitingCountDown] = useState<string>("");




    const dispatch = useDispatch();



    //Handle Update Count Down
    const updateCountDown = () => {
        /**
         * Get Remaning time passing starttime timestamp
         * @param {number} startTime - Unix timestamp
         * @param {number|undefined} addititionalMinutes
         * this method we are not passing additional miutes that so method will return difference
         * of current time's timestamp and starttime time stamp as object 
         * @returns {object} object will Inlcludes
         * difference - difference between timestamps
         * minutes - remaning minutes
         * seconds - remaning seconds
         * 
         */
        const t = getTimeRemaning(StartTime);
        // if time stamp's difference < 0  then need start new count down 
        //for wait 1 minute before removing race from list

        if (t.difference < 0) {
            /**
         * Get Remaning time passing starttime timestamp
         * @param {number} startTime - Unix timestamp
         * @param {number|undefined} addititionalMinutes
         * this method we are passing additional miutes that so method will add aditional minute to 
         * start time and get time difference between starttime and Current time to wait that additional minute
         * 
         * @returns {object} object will Inlcludes
         * difference - difference between timestamps
         * minutes - remaning minutes
         * seconds - remaning seconds
         * 
         */
            const wt = getTimeRemaning(StartTime, 1);

            //because of additional 1 minute, this if condition check t.seconds is equal 60 seconds
            //then it will clear the interval andremove the race from list
            //because requirement was if counddown completed the wait additional 1 minute to remove race from list

            if (wt.difference < 0) {

                //clear the Interval
                if(intervalId.current != null){
                    clearInterval(intervalId.current);

                }
                
                //remove race from list
                //dispatch method of RemoveRaceFromList to redux reducers to remove completed race
                //need to pass race_id that hold in intervalRaceId ref's current method's RaceID type
                if(intervalRaceId.current !=null){
                    dispatch(RemoveRaceFromList({
                        race_id: intervalRaceId.current
                    }));
                }
                


            } else {
                //create count down time string to display
                var time = `${wt.minutes > 0 ? wt.minutes + "m" : ""} ${wt.seconds ? wt.seconds + "s" : ""}`
                //Set state of Additional waiting Countdown
                SetWaitingCountDown(time);
                //remove orginal count down string from state
                SetCountDown("")
            }

        } else {
            //create count down time string to display
            var time = `${t.minutes > 0 ? t.minutes + "m" : ""} ${t.seconds ? t.seconds + "s" : ""}`
            //Setting count down state
            SetCountDown(time);
        }

    }

    //Start count down
    useLayoutEffect(() => {

        //get inital countdown value this will execute imidiate after component start
        updateCountDown();

        //create Setinterval method to run updateCountDown method to get countdown
        //it will wait 1s to run the method and update the countdown
        const id: ReturnType<typeof setInterval> = setInterval(
            updateCountDown
            , 1000)
        //set interval id in intervalId ref object to clear interval after countdowndown finish

        intervalId.current = id;



        //when component unmount interval automaticaly clearing handle
        return () => {
            if (intervalId.current) {
                clearInterval(intervalId.current);
            }
        }

    }, [])

    useEffect(() => {
        //after component get the race_id as props it'wll assign to intervalRaceId ref's current

        intervalRaceId.current = race_id;


    }, [race_id])




    return (

        <View>

            {
                WaitingCountDown ? <Text style={[styles.CountDownText, styles.WaitCoundDownText]}>{WaitingCountDown}</Text> : <View />
            }

            <Text style={styles.CountDownText}>{CountDown}</Text>

        </View>

    )
}

const styles = StyleSheet.create({
    CountDownText: {
        fontWeight: 'bold',
        color: Colors.secondary
    },
    WaitCoundDownText: {
        color: Colors.error
    }
})

export default CountDowncomponent;