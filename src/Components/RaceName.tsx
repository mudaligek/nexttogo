import React, { useState } from 'react';
import {
    View, Text, StyleSheet, useColorScheme, Image, Pressable
} from 'react-native';
import { Colors, FontSize, Padding } from '../Utils/Theme';


export type Props = {
    meeting_name: string;
    race_number: number;

}

/**
 * Display Race Name
 * @param {string} - meeting_name
 * @param {string} - race_number
 * @returns Element with Text
 */

const RaceNameComponent: React.FC<Props> = ({ meeting_name, race_number }: Props) => {
    //Get Screen color scheam to set background color
    const isDarkMode = useColorScheme() === 'dark';


    //set font color according to selected color scheam
    const fontColor = {
        color: isDarkMode ? Colors.Light : Colors.Dark,
    };

    return (
        <View style={[styles.container]}>

            <Text style={[fontColor]}>{`${meeting_name} ${race_number}`}</Text>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {

        justifyContent: 'center',
        flexDirection: 'row'
    }
});

export default RaceNameComponent;