import React, { useEffect } from 'react';
import {
    View, Text, StyleSheet, useColorScheme, FlatList
} from 'react-native';
import { Colors, FontSize, Padding } from '../Utils/Theme';
import CategoryItemComponent from './CategoryItem';
import { useSelector } from 'react-redux';
import { RootState } from '../Library/Store';

export type Props = {
    Title?: string;

}

/**
 * This hold the Header View which display the Category Title and Category list
 * @param {string} - Category Title 
 * Using Hard coded CategoryList in redux state rendinging the Horizontal Flatlist
 * @returns 
 */

const CategoryContainerComponent: React.FC<Props> = ({ Title }: Props) => {

    //Mobile Dark and Light Mode
    const isDarkMode = useColorScheme() === 'dark';
    //Redux Race state  
    const screenState = useSelector((state: RootState) => state.Race);



    //change font color according to Phone Dark and Light mode
    const fontColor = {
        color: isDarkMode ? Colors.Dark : Colors.Light,
    };

    //change backgroundColor according to Phone Dark and Light mode  
    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.primary : Colors.primary,
    };

    return (
        <View style={[styles.container, backgroundStyle]}>
            <View style={styles.left}>
                <Text style={[styles.titleText, fontColor]}>{Title}</Text>
            </View>

            <View style={styles.right}>

                <FlatList
                    horizontal
                    data={screenState.CategoryList}
                    extraData={screenState.CategoryList}
                    keyExtractor={item => item.category_id}
                    renderItem={({ item, index }) => <CategoryItemComponent
                        src={item.img_url}
                        category_id={item.category_id}
                        status={item.status}

                    />}
                />


            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        padding: Padding.small,
        flexDirection: 'row'
    },
    titleText: {
        fontSize: FontSize.small,
        fontWeight: '500',
        fontStyle:'italic'
    },
    left: {
        flex: 2
    },
    right: {
        flex: 2, alignItems: 'flex-end'
    }
});

export default CategoryContainerComponent;