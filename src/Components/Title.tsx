import React from 'react';
import {
    View, Text, StyleSheet, useColorScheme
} from 'react-native';
import { Colors, FontSize, Padding } from '../Utils/Theme';

export type Props = {
    Title: string;

}
/**
 * Home Screen Title Component
 * @param {string} - title
 * @returns element with title text
 */
const TitleComponent: React.FC<Props> = ({ Title }: Props) => {
    //Get Screen color scheam to set background color
    const isDarkMode = useColorScheme() === 'dark';

    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.Dark : Colors.Light,
    };
    //set font color according to selected color scheam
    const fontStyle = {
        color: isDarkMode ? Colors.Light : Colors.Dark,
    };

    return (
        <View style={[styles.container, backgroundStyle]}>
            <Text style={[styles.titleText, fontStyle]}>{Title}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: Padding.small
    },
    titleText: {
        fontSize: FontSize.large,
        fontWeight: '100'
    }
});

export default TitleComponent;