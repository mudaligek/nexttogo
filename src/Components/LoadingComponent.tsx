import React from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import { Colors } from '../Utils/Theme';
/**
 * 
 * @returns View with activity indicator zIndex set to 1 then user cannot select any UI elements untill loading finish
 */
const LoadingComponent = () => (
    <View style={styles.container}>
        <ActivityIndicator size={'large'} color={Colors.primary} />
    </View>
)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'rgba(52, 52, 52, 0.3)',
        position: 'absolute',
        zIndex: 1,
        left: 0, right: 0, top: 0, bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default LoadingComponent;