import React, { useState } from 'react';
import {
    View, Text, StyleSheet, useColorScheme, Image, Pressable
} from 'react-native';
import { Colors, FontSize, Padding } from '../Utils/Theme';
import { useDispatch, useSelector } from 'react-redux';
import { SetCategoryStatus } from '../Slice/Race';
import { RootState } from '../Library/Store';

export type Props = {
    src: any;
    category_id: string;
    status: boolean;
}

/**
 * Category List item returning Button With Image
 * @param {any} - src - include image format of 'require('hardCodedImageLocation')'
 * @param {string} - category_id - required to update category select unselect state
 * @param {boolean} - state - required to change Image Tint color.
 * @returns CategoryListItem
 */
const CategoryItemComponent: React.FC<Props> = ({ src, category_id, status }: Props) => {
    //Get Mobile Color scheme
    const isDarkMode = useColorScheme() === 'dark';
    const dispatch = useDispatch();
    //Get Redux Race State from root store
    const screenState = useSelector((state: RootState) => state.Race);

    //add custom background color using mobile Dark,Light Mode
    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.primary : Colors.primary,
    };
    //change Image tint color according to mobile dark List mode
    const ImagetintColor = {
        tintColor: isDarkMode ? Colors.Dark : Colors.Light,
    };
    //handle category list item select press
    //dispatch SetCategoryStatus method to change catogory state
    const _SetActive = () => {
        dispatch(SetCategoryStatus({
            status: !status,
            category_id: category_id
        }))
    }

    return (
        <View style={[styles.container, backgroundStyle]}>
            <Pressable disabled={screenState.Loading == 'pending' ? true : false} onPress={_SetActive}>
                <View style={styles.iconContainer}>
                <Image style={[styles.image, ImagetintColor, status && { tintColor: Colors.success }]} resizeMode='center' source={src} />
                </View>
            </Pressable>

        </View>
    )
}

const styles = StyleSheet.create({
    iconContainer:{
        alignItems:'center',
        justifyContent:'center'
    },
    container: {
        alignItems: 'flex-end',
        justifyContent: 'center',
        flexDirection: 'row'
    },
    image: {
        width: 40, height: 20
    }
});

export default CategoryItemComponent;