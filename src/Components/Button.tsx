import React from 'react';
import {
    View, Text, StyleSheet, TouchableNativeFeedback,
} from 'react-native';
import GlobalStyle from '../Utils/Styles';
import { Colors, FontSize, Height, Padding, Radius } from '../Utils/Theme';

type Props = {
    onPress: () => void,
    title: string
}

/**
 * 
 * Button component for custom button with Native Feed back
 * @param {()=>void} - onPress method for button press event
 * @param {string} - Button Title Text
 * @returns Button Element
 * 
 */

const Button = ({
    onPress, title
}: Props) => {
    return (
        <TouchableNativeFeedback onPress={onPress}>
            <View style={[styles.tryAgainBtn, GlobalStyle.shadow]}>
                <Text style={styles.btnText}>{title}</Text>
            </View>
        </TouchableNativeFeedback>

    )
}

const styles = StyleSheet.create({
    btnText: {
        color: 'white',
        fontWeight: 'bold'
    },
    tryAgainBtn: {
        backgroundColor: Colors.secondary,
        padding: Padding.small,
        borderRadius: Radius.large,
        height: Height.medium,
        width: 200, alignItems: 'center', justifyContent: 'center'
    },
})

export default Button;