import React, { useEffect, useState } from 'react';
import {
    View, Text, StyleSheet, FlatList,
} from 'react-native';
import RaceItemComponent from './RaceListItem';
import RaceNameComponent from './RaceName';
import { RootState } from '../Library/Store';
import { useDispatch, useSelector } from 'react-redux';
import { race_summarie } from '../Slice/Types';
import { fetchRaceList } from '../Slice/Race';
import LoadingComponent from './LoadingComponent';
import ItemSeparatorComponent from './ItemSeparatorComponent';
import { Padding } from '../Utils/Theme';

/**
 * Hold race List Flatlist
 * @returns 
 */
const RaceListComponent: React.FC = () => {

    const dispatch = useDispatch();
    //get Race state from redux state
    const screenState = useSelector((state: RootState) => state.Race);
    //initialize RaceList state
    const [RaceList, SetRaceList] = useState<race_summarie[]>([]);

    useEffect(() => {
        //load inital Race list
        //this redux method accepting limit of races
        dispatch(fetchRaceList(10));
    }, [])

    //If redux Race state's race_summaries or CategoryList updated then this will create the Race Listview again
    useEffect(() => {
        //create the race list view according to selected categories
        CreateRaceList()

    }, [screenState.race_summaries, screenState.CategoryList])



    //create the race list view according to selected categories
    const CreateRaceList = () => {
        //copy Race list array so we can change data
        let raceList = [...screenState.race_summaries];

        //sort accending using startTime timestamp
        raceList.sort(function (x, y) {
            return x.advertised_start.seconds - y.advertised_start.seconds;
        })


        //filter Selected Categories
        let SelCategories = screenState.CategoryList.filter(f => f.status);
        let FilterdRaceList: race_summarie[] = [];

        //check race inclued in selected categories and if its inclued then push into new array FilterdRaceList
        raceList.map(m => {
            //only push 5 races at a time
            if (FilterdRaceList?.length < 5) {

                if (SelCategories?.length > 0) {
                    let checkInwithCategory = SelCategories.find(f => f.category_id == m.category_id);

                    if (checkInwithCategory) {

                        FilterdRaceList.push(m)
                    }
                } else {
                    FilterdRaceList.push(m)
                }


            }
        })

        //if races are below 5 items then load new race list
        //and fill the 5 races in the list

        if (FilterdRaceList.length < 5) {
            let newLimit = screenState.race_summaries?.length + 10


            dispatch(fetchRaceList(newLimit));
            SetRaceList([])
        } else {
            //if races are equal 5 then set the SetRaceList as new race list
            SetRaceList(FilterdRaceList)
        }


    }

    return (
        <View style={[styles.container]}>

            {/**Loading */}
            {
                screenState.Loading == 'pending' && <LoadingComponent />
            }


            <View style={styles.listContainer}>
            <FlatList
            ItemSeparatorComponent={ItemSeparatorComponent}
                extraData={RaceList}
                data={RaceList}
                keyExtractor={(item) => item.race_id}
                renderItem={({ item }) => {
                    return (<RaceItemComponent
                        meeting_name={item.meeting_name}
                        race_number={item.race_number}
                        StartTime={item.advertised_start.seconds}
                        race_id={item.race_id}

                    />)
                }}
            />
            </View>




        </View>
    )
}

const styles = StyleSheet.create({
    listContainer:{
        padding:Padding.medium
    },
    container: {
        flex: 1
    }
});

export default RaceListComponent;